drupal-patches
==============

Patches for misc. drupal projects. These are mostly to handle patches that are published in a format  incompatible with Aegir 1.9, which uses Drush 4 and standalone Drush Make.
